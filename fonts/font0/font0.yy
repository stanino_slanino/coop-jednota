{
    "id": "2c7e85a8-4c0e-45fe-8a2e-6cfc9cad9dc0",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "font0",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Comic Sans MS",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "c0e18c16-f400-4a28-9d9c-b69d47c61df8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 149,
                "offset": 0,
                "shift": 46,
                "w": 46,
                "x": 670,
                "y": 606
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "214a5627-4d0d-480a-972b-1ad747351141",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 149,
                "offset": 4,
                "shift": 25,
                "w": 16,
                "x": 323,
                "y": 757
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "7dc2648e-9014-4a25-af52-af2a68d5e852",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 149,
                "offset": 3,
                "shift": 47,
                "w": 40,
                "x": 892,
                "y": 606
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "9d455fd5-6e02-4b3e-ba73-da8358eef807",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 149,
                "offset": -2,
                "shift": 90,
                "w": 91,
                "x": 300,
                "y": 2
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "585726a1-fa01-44ef-b1e5-90d88a8c1c5e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 149,
                "offset": 5,
                "shift": 74,
                "w": 64,
                "x": 69,
                "y": 304
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "a71facb2-1d45-477e-834f-f97eff140e86",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 149,
                "offset": 5,
                "shift": 88,
                "w": 82,
                "x": 485,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "2d23f233-8897-434b-80a1-99d5572ef281",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 149,
                "offset": 1,
                "shift": 70,
                "w": 68,
                "x": 593,
                "y": 153
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "ddfd8aaa-56e7-4931-a9bf-d13d6ddf8eca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 149,
                "offset": 15,
                "shift": 46,
                "w": 16,
                "x": 341,
                "y": 757
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "c94a4bd9-8314-4d01-b279-aaa7c8c39f6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 149,
                "offset": 3,
                "shift": 39,
                "w": 36,
                "x": 79,
                "y": 757
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "76efca44-57cb-4162-8840-bd64801ade51",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 149,
                "offset": 3,
                "shift": 39,
                "w": 36,
                "x": 41,
                "y": 757
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "442c8709-b584-46c6-a265-429846e22f81",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 149,
                "offset": 0,
                "shift": 57,
                "w": 53,
                "x": 575,
                "y": 455
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "34afa0a6-06fd-430c-a1ef-669248873ebb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 149,
                "offset": 8,
                "shift": 65,
                "w": 51,
                "x": 267,
                "y": 606
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "ddf57e6f-f973-4fbf-98b7-a1929fc573a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 149,
                "offset": 11,
                "shift": 46,
                "w": 21,
                "x": 239,
                "y": 757
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "43d47e89-2190-4f25-adc0-c2ead7fc4052",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 149,
                "offset": 11,
                "shift": 65,
                "w": 43,
                "x": 718,
                "y": 606
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "870753b8-3e22-4587-8cc5-0f8ec3a48fff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 149,
                "offset": 14,
                "shift": 46,
                "w": 18,
                "x": 283,
                "y": 757
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "23886e30-4801-4e7e-b3c3-536537c690a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 149,
                "offset": 2,
                "shift": 55,
                "w": 51,
                "x": 320,
                "y": 606
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "8108a7f6-17be-472a-b865-707b0de1c9ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 149,
                "offset": 1,
                "shift": 65,
                "w": 63,
                "x": 200,
                "y": 304
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "e063d01c-7c7b-48c7-8da3-76c160923f25",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 149,
                "offset": 12,
                "shift": 65,
                "w": 39,
                "x": 976,
                "y": 606
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "f7d391b7-5b09-484e-b177-ea5de9258c84",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 149,
                "offset": 6,
                "shift": 65,
                "w": 54,
                "x": 463,
                "y": 455
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "bb6ac3cc-d241-43c8-9433-c97e26f7e83a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 149,
                "offset": 5,
                "shift": 65,
                "w": 54,
                "x": 407,
                "y": 455
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "3aa5b2aa-fb68-4937-99eb-4397de8e86f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 149,
                "offset": 0,
                "shift": 65,
                "w": 65,
                "x": 2,
                "y": 304
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "d18b9c3a-e116-4748-9d7a-47cefd1f62e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 149,
                "offset": 4,
                "shift": 65,
                "w": 59,
                "x": 765,
                "y": 304
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "4e10fac4-a0fb-4944-8cbf-d2d8861f07fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 149,
                "offset": 4,
                "shift": 65,
                "w": 58,
                "x": 887,
                "y": 304
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "7953b38d-4bc1-4119-89f2-988ffa589a8d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 149,
                "offset": 1,
                "shift": 65,
                "w": 65,
                "x": 941,
                "y": 153
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "10d4b418-43cb-4709-9f8a-8a8b1bd04813",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 149,
                "offset": 4,
                "shift": 65,
                "w": 57,
                "x": 947,
                "y": 304
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "35832f10-0328-412e-bd7e-ab65bd2405ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 149,
                "offset": 2,
                "shift": 65,
                "w": 60,
                "x": 520,
                "y": 304
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "925c7d23-f722-4576-a8cc-842e49c1c04c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 149,
                "offset": 15,
                "shift": 46,
                "w": 18,
                "x": 303,
                "y": 757
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "07773dbd-cb54-4403-8736-ee4780068da0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 149,
                "offset": 11,
                "shift": 46,
                "w": 22,
                "x": 215,
                "y": 757
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "3e02d84f-249f-46ae-b626-d5d03a2143ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 149,
                "offset": 12,
                "shift": 65,
                "w": 37,
                "x": 2,
                "y": 757
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "9b871783-7e04-429f-9589-f801e4d37890",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 149,
                "offset": 9,
                "shift": 65,
                "w": 46,
                "x": 622,
                "y": 606
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "6ce7f275-9084-46b3-9518-c96520939dd3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 149,
                "offset": 15,
                "shift": 65,
                "w": 40,
                "x": 934,
                "y": 606
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "6b88c613-f0bf-411e-8812-73d2dec0edfa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 149,
                "offset": 4,
                "shift": 61,
                "w": 53,
                "x": 630,
                "y": 455
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "2ad16756-0135-4e36-82c4-8fbec224384c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 149,
                "offset": 3,
                "shift": 100,
                "w": 91,
                "x": 207,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "e084756e-d3f2-42e4-b936-05848c151749",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 149,
                "offset": 4,
                "shift": 78,
                "w": 69,
                "x": 522,
                "y": 153
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "c9009aad-57a8-45d1-9869-6e83b7e647ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 149,
                "offset": 6,
                "shift": 67,
                "w": 59,
                "x": 582,
                "y": 304
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "d6a958d2-9174-4bc4-a3ca-26037ac4b9f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 149,
                "offset": 2,
                "shift": 66,
                "w": 63,
                "x": 265,
                "y": 304
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "e4f83d65-d1f4-407e-a459-ee6f7df48f3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 149,
                "offset": 7,
                "shift": 77,
                "w": 67,
                "x": 803,
                "y": 153
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "ef4fa90b-88ed-4cde-be35-0e6a80353eac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 149,
                "offset": 4,
                "shift": 67,
                "w": 62,
                "x": 330,
                "y": 304
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "c86a8289-6502-4b35-a83d-b8e30a4af42e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 149,
                "offset": 4,
                "shift": 65,
                "w": 59,
                "x": 643,
                "y": 304
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "b7b7c39b-0d46-4ec0-8bc7-fbe31a279ea2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 149,
                "offset": 2,
                "shift": 73,
                "w": 71,
                "x": 305,
                "y": 153
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "935d0cc5-be21-4154-b875-92ec8040d02e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 149,
                "offset": 5,
                "shift": 82,
                "w": 74,
                "x": 79,
                "y": 153
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "c226dda7-890b-4441-84d8-39378cb98554",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 149,
                "offset": 1,
                "shift": 58,
                "w": 57,
                "x": 61,
                "y": 455
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "04b1d5be-ef58-41bb-ab64-71e58933baef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 149,
                "offset": 2,
                "shift": 71,
                "w": 68,
                "x": 733,
                "y": 153
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "75277b40-d67a-401e-a55f-9c5a406842e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 149,
                "offset": 5,
                "shift": 65,
                "w": 59,
                "x": 826,
                "y": 304
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "cffc8b35-9bcf-4d64-8ce4-672d7159a9ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 149,
                "offset": 3,
                "shift": 59,
                "w": 56,
                "x": 236,
                "y": 455
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "14fc3868-e189-4ba7-864e-5fc0f43b18fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 149,
                "offset": 3,
                "shift": 94,
                "w": 90,
                "x": 393,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "49c994eb-0c49-442e-89c2-5e2deafaf744",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 149,
                "offset": 4,
                "shift": 87,
                "w": 80,
                "x": 569,
                "y": 2
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "6129e5e4-59b1-4d33-bae5-cc4db801b885",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 149,
                "offset": 3,
                "shift": 85,
                "w": 80,
                "x": 651,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "502605c0-51e8-49c4-86f7-96fcfc8e05b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 149,
                "offset": 3,
                "shift": 57,
                "w": 52,
                "x": 849,
                "y": 455
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "e8e376c6-3e75-4d52-9b6e-01f024cfe8f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 149,
                "offset": 1,
                "shift": 94,
                "w": 93,
                "x": 112,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "076575fe-90e4-4a52-9a19-948de9ce62c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 149,
                "offset": 4,
                "shift": 69,
                "w": 63,
                "x": 135,
                "y": 304
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "da959f8c-b590-4e9c-93d1-8980cfcdafa8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 149,
                "offset": 2,
                "shift": 74,
                "w": 70,
                "x": 378,
                "y": 153
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "74c74123-53cb-456d-aea6-cdd9484bab4a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 149,
                "offset": 0,
                "shift": 74,
                "w": 76,
                "x": 891,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "025a2cad-b816-42aa-ae67-67166db4d702",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 149,
                "offset": 4,
                "shift": 79,
                "w": 70,
                "x": 450,
                "y": 153
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "68e2f2fc-12b0-4ffb-add6-8bd757e987d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 149,
                "offset": 3,
                "shift": 72,
                "w": 67,
                "x": 872,
                "y": 153
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "b23108d0-8a64-48f9-94fa-e0d04a120c1d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 149,
                "offset": 2,
                "shift": 111,
                "w": 108,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "a9a6c556-022e-4417-928c-4521d03d6f17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 149,
                "offset": 0,
                "shift": 77,
                "w": 75,
                "x": 2,
                "y": 153
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "1bce8291-aa66-402b-9e6f-2aae6de133d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 149,
                "offset": -1,
                "shift": 68,
                "w": 68,
                "x": 663,
                "y": 153
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "c4ee3bdc-0592-46b7-9a9f-887308bd8037",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 149,
                "offset": 0,
                "shift": 74,
                "w": 74,
                "x": 155,
                "y": 153
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "90ffb6f4-498d-41e4-8540-aade0ad46c66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 149,
                "offset": 7,
                "shift": 40,
                "w": 32,
                "x": 151,
                "y": 757
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "2d62f2e7-0b2e-408d-a61c-63acf0ca29b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 149,
                "offset": 7,
                "shift": 59,
                "w": 49,
                "x": 373,
                "y": 606
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "0d448af5-5197-42ca-ac1d-606c1be30093",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 149,
                "offset": 3,
                "shift": 40,
                "w": 32,
                "x": 117,
                "y": 757
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "836641b5-5b98-4c69-a676-1a12eb8c907e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 149,
                "offset": 9,
                "shift": 65,
                "w": 48,
                "x": 475,
                "y": 606
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "50dbb320-bfcd-40c5-9e07-04c08b632c04",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 149,
                "offset": -4,
                "shift": 67,
                "w": 76,
                "x": 813,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "d2eede53-4053-41fe-9bef-fb312e8f052d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 149,
                "offset": 5,
                "shift": 60,
                "w": 28,
                "x": 185,
                "y": 757
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "a5f0666a-b7bd-45e6-992f-20ca8add253d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 149,
                "offset": 2,
                "shift": 59,
                "w": 56,
                "x": 120,
                "y": 455
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "3a317fab-2f5c-4b79-809d-4fc0f9fed717",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 149,
                "offset": 4,
                "shift": 63,
                "w": 56,
                "x": 178,
                "y": 455
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "b0627950-d04b-42c3-82cb-b7c3aa7bf432",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 149,
                "offset": 1,
                "shift": 55,
                "w": 51,
                "x": 2,
                "y": 606
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "5442b968-ca3d-4483-908d-70c72774cbba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 149,
                "offset": 2,
                "shift": 63,
                "w": 57,
                "x": 2,
                "y": 455
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "2b081f04-e808-473d-925a-c3f588226ad0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 149,
                "offset": 2,
                "shift": 60,
                "w": 54,
                "x": 351,
                "y": 455
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "00b0db81-6bd5-42b0-8975-fa5182d15409",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 149,
                "offset": 1,
                "shift": 54,
                "w": 51,
                "x": 108,
                "y": 606
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "21d4e99f-1242-4ff5-9499-60cbf9835ac3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 149,
                "offset": 0,
                "shift": 57,
                "w": 55,
                "x": 294,
                "y": 455
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "15e727f1-eab9-4943-a348-e7c868bd2e0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 149,
                "offset": 5,
                "shift": 62,
                "w": 54,
                "x": 519,
                "y": 455
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "07845743-880c-4c6d-b35a-2bd09b572d72",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 149,
                "offset": 7,
                "shift": 30,
                "w": 19,
                "x": 262,
                "y": 757
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "518e36a7-6643-42c8-ac55-3d29c335c6cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 149,
                "offset": -4,
                "shift": 43,
                "w": 41,
                "x": 849,
                "y": 606
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "c4f17d95-c1a7-47bf-b0cc-41cec781080c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 149,
                "offset": 4,
                "shift": 58,
                "w": 53,
                "x": 740,
                "y": 455
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "2f115ee1-4b1a-45da-9107-051395b2da6e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 149,
                "offset": 7,
                "shift": 29,
                "w": 15,
                "x": 359,
                "y": 757
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "16bd99e2-fb5b-404f-8c78-38048aa995c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 149,
                "offset": 4,
                "shift": 83,
                "w": 78,
                "x": 733,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "20101f27-e98e-4dd6-a7f0-1667f2db2577",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 149,
                "offset": 3,
                "shift": 56,
                "w": 51,
                "x": 214,
                "y": 606
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "3b9bb755-42ce-404c-a7b9-f7ee4ba956d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 149,
                "offset": 1,
                "shift": 56,
                "w": 52,
                "x": 903,
                "y": 455
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "3c4eed55-e045-4889-9150-ce94525afb76",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 149,
                "offset": 4,
                "shift": 57,
                "w": 51,
                "x": 55,
                "y": 606
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "33239f16-b1bd-4247-9bef-eed7ce823c7b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 149,
                "offset": 1,
                "shift": 56,
                "w": 51,
                "x": 957,
                "y": 455
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "79057e77-7539-4c10-8e63-6d9acf809ffe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 149,
                "offset": 4,
                "shift": 51,
                "w": 46,
                "x": 574,
                "y": 606
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "b0de46df-713a-4d26-a4f5-e469c3d6ac8d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 149,
                "offset": 2,
                "shift": 52,
                "w": 47,
                "x": 525,
                "y": 606
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "fcf00ea4-74c7-42a4-aad3-2430a9b784d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 149,
                "offset": 1,
                "shift": 50,
                "w": 49,
                "x": 424,
                "y": 606
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "861685f8-2487-4fc3-886b-e66dca78dc24",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 149,
                "offset": 2,
                "shift": 56,
                "w": 51,
                "x": 161,
                "y": 606
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "23cc56a3-06df-477d-b981-7567a2c5798b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 149,
                "offset": 1,
                "shift": 52,
                "w": 52,
                "x": 795,
                "y": 455
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "71fd43eb-801f-4754-8bc9-213ac4d3274e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 149,
                "offset": 1,
                "shift": 73,
                "w": 72,
                "x": 231,
                "y": 153
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "ce82e2e4-7eea-4953-bf25-ab506dc73240",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 149,
                "offset": 1,
                "shift": 63,
                "w": 62,
                "x": 394,
                "y": 304
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "14f52a3f-5726-4060-a394-195652e4bc2d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 149,
                "offset": 0,
                "shift": 59,
                "w": 59,
                "x": 704,
                "y": 304
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "ca591b48-9227-4957-8c88-9160e08cadb1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 149,
                "offset": 2,
                "shift": 58,
                "w": 53,
                "x": 685,
                "y": 455
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "f7b356dd-0e3a-409b-bdb4-92a2345d15df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 149,
                "offset": -2,
                "shift": 39,
                "w": 41,
                "x": 806,
                "y": 606
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "8290a73f-7c9a-4fe5-a30c-d46af8976836",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 149,
                "offset": 16,
                "shift": 45,
                "w": 14,
                "x": 376,
                "y": 757
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "3a1b0a2e-a223-4fce-a00c-55c005e21734",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 149,
                "offset": -2,
                "shift": 39,
                "w": 41,
                "x": 763,
                "y": 606
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "d2b92666-54a3-4e4d-bc66-384eb0fef246",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 149,
                "offset": 2,
                "shift": 65,
                "w": 60,
                "x": 458,
                "y": 304
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 80,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}