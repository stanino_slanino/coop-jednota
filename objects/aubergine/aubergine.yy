{
    "id": "53efc764-418d-4145-8966-f826f206d8b1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "aubergine",
    "eventList": [
        {
            "id": "5d7a7dd9-2cb1-4b83-88de-56f4ffbdceb9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "53efc764-418d-4145-8966-f826f206d8b1"
        },
        {
            "id": "0ad873a5-f5de-4df2-bcf8-f8834d54888b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "53efc764-418d-4145-8966-f826f206d8b1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "93614bec-f304-439e-8d2a-a0ad85b36ecd",
    "visible": true
}