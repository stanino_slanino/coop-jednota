{
    "id": "15219783-bda0-4190-b8f4-37f7c52676c6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "can",
    "eventList": [
        {
            "id": "594d2398-fa57-4cde-87cb-f923de67e3cc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "15219783-bda0-4190-b8f4-37f7c52676c6"
        },
        {
            "id": "07284736-6316-4ede-aef9-3f381c1fda64",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "15219783-bda0-4190-b8f4-37f7c52676c6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "32f9c303-6118-441e-97a5-648b7ca49d70",
    "visible": true
}