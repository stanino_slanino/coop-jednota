/// @description Insert description here
// You can write your code in this editor

//x = room_height;
//y = room_width/2;

global.players = array_create(4);
global.cart_inst = self;

global.my_health = 10;
global.my_score = 0;


audio_play_sound(kolieska_new, 2, true);

player0_cooldown = false;

vertical_speed = 0;
default_bump = 15;
left_bump = 10;
right_bump = 10;
speed_increment = 2;
player_ctr = 0;
gamepad_count = gamepad_get_device_count();

frames_to_next_reset_left = 0;
frames_to_next_reset_right = 0;

combo_bonus_time = 15;

player_inst = instance_create_layer(0,0,"Instances",player);
player_inst.controller_id = 4;
global.players[0] = player_inst;

player_inst = instance_create_layer(0,0,"Instances",player);
player_inst.controller_id = 5;
global.players[1] = player_inst;

player_inst = instance_create_layer(0,0,"Instances",player);
player_inst.controller_id = 6;
global.players[2] = player_inst;

player_inst = instance_create_layer(0,0,"Instances",player);
player_inst.controller_id = 7;
global.players[3] = player_inst;