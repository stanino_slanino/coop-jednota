/// @description Insert description here
// You can write your code in this editor



x += vertical_speed;

global.my_score += 1;

if (global.my_health <= 0) {
	audio_stop_sound(kolieska_new);
	room_goto(end_room);	
}

left_pressed = false;
right_pressed = false;

press_count_left = 0;
press_count_right = 0;

for (var i = 0; i < gamepad_count-1; i++) {
	for (var p = 0; p < 4; p++) {
		if (global.players[p].controller_id == i) {
			player_inst = global.players[p];
			vegetable_inst = player_inst.vegetable;
		}
	}
	
	
	if gamepad_is_connected(i) {
		//show_debug_message(string(i));
		//LEFT
		if (gamepad_button_check_pressed(i, 19) && player_inst.can_move) {
			left_pressed = true;
			image_angle -= 10;
			vegetable_inst.image_angle += vegetable_inst.bump_angle;
			left_bump += default_bump;
			left_bump = clamp(left_bump, 0, 60);
			frames_to_next_reset_left += combo_bonus_time;
			frames_to_next_reset_right = 0;
			player_inst.can_move = false;
			set_player_cooldown(player_inst);
			play_vegetable_sound(player_inst, "left");
		}
		//RIGHT
		if (gamepad_button_check_pressed(i, 20)  && player_inst.can_move) {
			right_pressed = true;
			image_angle += 10;
			vegetable_inst.image_angle -= vegetable_inst.bump_angle;
			right_bump += default_bump;
			rigth_bump = clamp(right_bump, 0, 60);
			frames_to_next_reset_right += combo_bonus_time;
			frames_to_next_reset_left = 0;
			player_inst.can_move = false;
			set_player_cooldown(player_inst);
			play_vegetable_sound(player_inst, "right");
		}
	}
}




if right_pressed {
	x += right_bump;
	vertical_speed += speed_increment;	
}

if left_pressed {
	x -= left_bump;
	vertical_speed -= speed_increment;
}

x = clamp(x, 200, room_width - 200);

abs_vertical_speed = abs(vertical_speed);
if (abs_vertical_speed > 1) {
	vertical_speed = vertical_speed*0.99;
} else {
	vertical_speed = 0;
}

if (abs(image_angle) > 1) {
	image_angle = lerp(image_angle,0,0.4);
}

if (frames_to_next_reset_left > 0) {
	frames_to_next_reset_left -= 1;
} else {
	left_bump=default_bump;	
}

if (frames_to_next_reset_right > 0) {
	frames_to_next_reset_right -= 1;
} else {
	right_bump=default_bump;
}

//show_debug_message("frames_to_next_reset_left: " + string(frames_to_next_reset_left));
//show_debug_message("frames_to_next_reset_right: " + string(frames_to_next_reset_left));
//show_debug_message("right_bump: " + string(right_bump));
//show_debug_message("left_bump: " + string(left_bump));