{
    "id": "7928898c-c455-4717-91ad-2451739e0749",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "cart",
    "eventList": [
        {
            "id": "6c1161a9-33dd-48d3-bba8-c03d605feec0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "7928898c-c455-4717-91ad-2451739e0749"
        },
        {
            "id": "d236538b-e954-41e8-a2f0-3c4ad9c25777",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7928898c-c455-4717-91ad-2451739e0749"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 2,
    "physicsShapePoints": [
        {
            "id": "d63f34c0-b3ee-4ece-b500-4cc0cef74060",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 212.545044,
            "y": 131.8964
        },
        {
            "id": "7acc6aa8-3d7a-4461-bd95-23c826611087",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 507.459473,
            "y": 133.378372
        },
        {
            "id": "fa40106c-66a0-4b1e-84c5-d7aa85823b0c",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 569.7027,
            "y": 326.036
        },
        {
            "id": "29dffb19-f15e-41ff-be25-8b5463f1db27",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 156.229736,
            "y": 326.482
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "bd1b9d1e-a759-4898-b576-f72e5a1ee685",
    "visible": true
}