{
    "id": "eef58b03-649e-4254-bad2-196a20b35fbf",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "end_screen",
    "eventList": [
        {
            "id": "21b95aab-1b73-4271-a9d4-3b94d869417a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "eef58b03-649e-4254-bad2-196a20b35fbf"
        },
        {
            "id": "c552b222-0dfd-4e20-95cc-a718c2b63a65",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "eef58b03-649e-4254-bad2-196a20b35fbf"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "87761601-a077-48d4-bb58-626db7597115",
    "visible": true
}