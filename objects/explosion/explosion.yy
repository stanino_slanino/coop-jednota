{
    "id": "7f45cdb1-526b-48a2-b63a-cbabc41126af",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "explosion",
    "eventList": [
        {
            "id": "4de87fe0-ff2c-45d9-a18e-7ccee27bd2ca",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7f45cdb1-526b-48a2-b63a-cbabc41126af"
        },
        {
            "id": "8d26d558-4c04-43d1-b009-a5601dfe7c65",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "7f45cdb1-526b-48a2-b63a-cbabc41126af"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "70093695-662b-405a-8dea-4979eade46fc",
    "visible": true
}