{
    "id": "ae34b539-b570-4c37-84fc-8f108446472f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "hurdle_1",
    "eventList": [
        {
            "id": "12f0adf7-ca36-4573-ae15-2e505e825f4b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ae34b539-b570-4c37-84fc-8f108446472f"
        },
        {
            "id": "9147b33b-1c30-4777-a45b-f37bc857a212",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ae34b539-b570-4c37-84fc-8f108446472f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "cb4246d3-f539-4213-8075-fd12a76b8e98",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "2",
            "varName": "hurdle_width",
            "varType": 0
        }
    ],
    "solid": false,
    "spriteId": "c17ab4a3-ae81-46ba-bc32-8af1cd195a70",
    "visible": true
}