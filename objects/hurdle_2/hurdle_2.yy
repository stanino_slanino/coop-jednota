{
    "id": "b577a540-a63b-4f36-84cc-1be0c3533be2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "hurdle_2",
    "eventList": [
        {
            "id": "94496a3e-f83c-46d4-bed8-7a221a6c10c7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b577a540-a63b-4f36-84cc-1be0c3533be2"
        },
        {
            "id": "67810e63-0761-4db9-91ff-eee05a9fada5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b577a540-a63b-4f36-84cc-1be0c3533be2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "4b83e223-1090-48e7-aeb1-03d58fabba84",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "3",
            "varName": "hurdle_width",
            "varType": 0
        }
    ],
    "solid": false,
    "spriteId": "6f37d94f-461d-4689-b822-229c1d6cbbd3",
    "visible": true
}