/// @description Spawns hurdles
// You can write your code in this editor

//to_create = irandom_range(0,2);

//if to_create = 0 {
//	instance_create_layer(0,0,"Hurdles",hurdle_pool[0]);
//}
//if to_create = 1 {
//	instance_create_layer(0,0,"Hurdles",hurdle_pool[1]);
//}
//if to_create = 2 {
//	instance_create_layer(0,0,"Hurdles",hurdle_pool[2]);
//}

//new_alarm = irandom_range(room_speed*0.5, room_speed);


for (var h = 0; h < row_limit; h++) {
	new_hurdle = real(ds_grid_get(level1, h, current_row));
	//show_debug_message(new_hurdle);
	if (new_hurdle != -1) {
		var hurdle_inst = instance_create_layer(0,0,"Hurdles",hurdle_pool[new_hurdle]);
		script_execute(assign_lane, hurdle_inst, h);
	}
}



if (current_row == 0) {
	current_row = level_limit;
} else {
	current_row--;
}

new_alarm = room_speed*0.5;
alarm_set(0, new_alarm);