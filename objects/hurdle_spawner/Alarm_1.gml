/// @description Spawns shelves
// You can write your code in this editor

to_create = irandom_range(0,1);

if to_create = 0 {
	instance_create_layer(0,0,"Shelves",shelf_left);
}
if to_create = 1 {
	instance_create_layer(0,0,"Shelves",shelf_right);
}

new_alarm = irandom_range(room_speed*0.5, room_speed);

alarm_set(1, 2 * room_speed);