{
    "id": "dd0768fa-8a6a-4647-8cbe-b6a2ec86c5b4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "hurdle_spawner",
    "eventList": [
        {
            "id": "b5c2dfb1-1fc5-416b-83f2-70a7a20d118e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "dd0768fa-8a6a-4647-8cbe-b6a2ec86c5b4"
        },
        {
            "id": "e94aea3f-82b5-41b7-9911-2d95b1ddeb2b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "dd0768fa-8a6a-4647-8cbe-b6a2ec86c5b4"
        },
        {
            "id": "f9c4bc6e-c88f-40e2-b245-91b10a13f8a1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "dd0768fa-8a6a-4647-8cbe-b6a2ec86c5b4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}