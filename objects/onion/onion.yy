{
    "id": "3e724353-4fe5-4c19-9779-dae311fe08de",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "onion",
    "eventList": [
        {
            "id": "848efaf1-13c0-4827-bbaa-7969192ea49c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3e724353-4fe5-4c19-9779-dae311fe08de"
        },
        {
            "id": "9c959f70-8af1-4b6b-b290-0b6575fba8b0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "3e724353-4fe5-4c19-9779-dae311fe08de"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "198925f2-d3b4-4c8c-8188-44eb8ea8dac0",
    "visible": true
}