{
    "id": "6d565bee-9488-4a8a-8346-77e361145215",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "shelf_left",
    "eventList": [
        {
            "id": "628d9fa4-30dc-4e77-b23d-063e1e1a80d9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6d565bee-9488-4a8a-8346-77e361145215"
        },
        {
            "id": "5dd13fed-8987-4023-95f4-fed0f4837d7a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6d565bee-9488-4a8a-8346-77e361145215"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "737b9fad-85ef-4969-8f7a-cac40a507e3c",
    "visible": true
}