{
    "id": "f98c91c3-6bf6-4041-9d9e-5f7cd1b4422c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "shelf_right",
    "eventList": [
        {
            "id": "0caf55fa-a111-4bf2-bb08-7234cb2bc5a3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f98c91c3-6bf6-4041-9d9e-5f7cd1b4422c"
        },
        {
            "id": "9873cdf8-dcbe-4dc8-8d0e-79a3442d914c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f98c91c3-6bf6-4041-9d9e-5f7cd1b4422c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2cdbba12-467d-40b2-a81f-e28cf5add464",
    "visible": true
}