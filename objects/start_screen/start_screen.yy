{
    "id": "e1030b52-3112-4f2c-b152-335858539af2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "start_screen",
    "eventList": [
        {
            "id": "7656160f-6260-44fa-b6f1-e3f62a220d45",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e1030b52-3112-4f2c-b152-335858539af2"
        },
        {
            "id": "82638986-fea0-4ba2-950e-57d953fa8eab",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e1030b52-3112-4f2c-b152-335858539af2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "78c5cfd9-e3fc-4013-9e64-2bd63ea56cc2",
    "visible": true
}