hurdle_size = lerp(0,1,y/room_height);
image_xscale = hurdle_size;
image_yscale = hurdle_size;
speed += lerp(0.1, 0.7, y/room_height);

if (y >= room_height) {
	image_alpha -= 0.1;
}

if (image_alpha <= 0) {
	instance_destroy();
}

place = place_meeting(x + hspeed, y + vspeed, cart);

if ((place != 0) && !hit) {
	hit = true;
	instance_create_layer(x, y, "Explosions", explosion);	
	global.my_health -= 1;
	audio_play_sound(naraz_vozika, 3, false);
}

//show_debug_message(string(place));