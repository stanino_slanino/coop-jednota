x = cart_inst.x + offset_x;
y = cart_inst.y + offset_y;

if (abs(image_angle) > 1) {
	//show_debug_message(string(image_angle));
	angle_sign = - sign(image_angle);
	image_angle += (bump_angle/player_inst.default_cooldown) * angle_sign;
} else {
	image_angle = 0;
}