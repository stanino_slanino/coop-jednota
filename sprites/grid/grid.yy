{
    "id": "5fb3a8b7-20a9-4d44-9896-e6378adc7d90",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "grid",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1079,
    "bbox_left": 0,
    "bbox_right": 1919,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b241bec1-5776-4ebd-b7f7-ed43bfd9b236",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5fb3a8b7-20a9-4d44-9896-e6378adc7d90",
            "compositeImage": {
                "id": "b0a3729f-206e-4731-a67f-7c015a91f527",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b241bec1-5776-4ebd-b7f7-ed43bfd9b236",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e59e45d-cb93-44cb-9005-8c55b490501d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b241bec1-5776-4ebd-b7f7-ed43bfd9b236",
                    "LayerId": "90f49e17-6eef-491e-8a7e-97dc6f3e59ae"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1080,
    "layers": [
        {
            "id": "90f49e17-6eef-491e-8a7e-97dc6f3e59ae",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5fb3a8b7-20a9-4d44-9896-e6378adc7d90",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1920,
    "xorig": 0,
    "yorig": 0
}