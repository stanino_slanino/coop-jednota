{
    "id": "bd1b9d1e-a759-4898-b576-f72e5a1ee685",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "kosikk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 335,
    "bbox_left": 237,
    "bbox_right": 487,
    "bbox_top": 133,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4736309d-0045-4cd3-8bec-e0067e7439d2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bd1b9d1e-a759-4898-b576-f72e5a1ee685",
            "compositeImage": {
                "id": "9f99fc31-080f-4343-9959-9230d794fbd9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4736309d-0045-4cd3-8bec-e0067e7439d2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5bd1c4d-5501-412b-9996-f6c56d574b34",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4736309d-0045-4cd3-8bec-e0067e7439d2",
                    "LayerId": "184a6f5f-2f2d-4d20-9097-03a4bc7bb324"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 329,
    "layers": [
        {
            "id": "184a6f5f-2f2d-4d20-9097-03a4bc7bb324",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bd1b9d1e-a759-4898-b576-f72e5a1ee685",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 721,
    "xorig": 360,
    "yorig": 328
}