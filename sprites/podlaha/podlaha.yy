{
    "id": "570433c3-e6d6-408a-a432-498a321906c9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "podlaha",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1079,
    "bbox_left": 0,
    "bbox_right": 1919,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "61540aac-0f60-4020-abc5-dd744b823f76",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "570433c3-e6d6-408a-a432-498a321906c9",
            "compositeImage": {
                "id": "78a87621-743f-4528-a4f7-3f0724f1f0bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "61540aac-0f60-4020-abc5-dd744b823f76",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d682ce2f-4f0e-45dc-9356-304a30f88fd3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "61540aac-0f60-4020-abc5-dd744b823f76",
                    "LayerId": "4851ccf7-6d51-4341-a0b7-21749b78058e"
                }
            ]
        },
        {
            "id": "f0b7d360-f7f1-4e09-b944-fe299a3a179b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "570433c3-e6d6-408a-a432-498a321906c9",
            "compositeImage": {
                "id": "5be664d2-9bca-4218-8e23-70b32bd6a828",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0b7d360-f7f1-4e09-b944-fe299a3a179b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9283439-74c6-4caf-a73e-e04d41fbeafe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0b7d360-f7f1-4e09-b944-fe299a3a179b",
                    "LayerId": "4851ccf7-6d51-4341-a0b7-21749b78058e"
                }
            ]
        },
        {
            "id": "a90652b4-c75e-4b5c-b2c1-700121a95fb3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "570433c3-e6d6-408a-a432-498a321906c9",
            "compositeImage": {
                "id": "92e4c4aa-7f86-4c6f-845f-7fa2235978c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a90652b4-c75e-4b5c-b2c1-700121a95fb3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "871a7fb1-49ad-475b-9bcf-2101df20e855",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a90652b4-c75e-4b5c-b2c1-700121a95fb3",
                    "LayerId": "4851ccf7-6d51-4341-a0b7-21749b78058e"
                }
            ]
        },
        {
            "id": "cf47cf37-ec8c-4242-8d57-2a8d1895493d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "570433c3-e6d6-408a-a432-498a321906c9",
            "compositeImage": {
                "id": "8a7c5b7f-a2e6-4362-a58b-bbac81c494aa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf47cf37-ec8c-4242-8d57-2a8d1895493d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d92f9ac-accf-48a4-b17b-99831b404282",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf47cf37-ec8c-4242-8d57-2a8d1895493d",
                    "LayerId": "4851ccf7-6d51-4341-a0b7-21749b78058e"
                }
            ]
        },
        {
            "id": "38160ee5-d93f-40cd-b26b-b07b5adc5293",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "570433c3-e6d6-408a-a432-498a321906c9",
            "compositeImage": {
                "id": "700c0bb9-f3c7-4457-a2b7-d93bb0f9114a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38160ee5-d93f-40cd-b26b-b07b5adc5293",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ebb96a6-a36d-4434-9e15-5d5f28d52a65",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38160ee5-d93f-40cd-b26b-b07b5adc5293",
                    "LayerId": "4851ccf7-6d51-4341-a0b7-21749b78058e"
                }
            ]
        },
        {
            "id": "501b7d7d-94fe-4cd3-9d42-4fb053180af8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "570433c3-e6d6-408a-a432-498a321906c9",
            "compositeImage": {
                "id": "abbf69db-24d0-46a0-a054-378f71b3fc86",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "501b7d7d-94fe-4cd3-9d42-4fb053180af8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7b90c5f-9c10-46fd-8e55-59141942a8fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "501b7d7d-94fe-4cd3-9d42-4fb053180af8",
                    "LayerId": "4851ccf7-6d51-4341-a0b7-21749b78058e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1080,
    "layers": [
        {
            "id": "4851ccf7-6d51-4341-a0b7-21749b78058e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "570433c3-e6d6-408a-a432-498a321906c9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1920,
    "xorig": 0,
    "yorig": 0
}