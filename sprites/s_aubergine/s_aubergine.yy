{
    "id": "93614bec-f304-439e-8d2a-a0ad85b36ecd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_aubergine",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 312,
    "bbox_left": 0,
    "bbox_right": 202,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a46a8891-5b5f-4f7c-bdf3-2f3f3620bb5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "93614bec-f304-439e-8d2a-a0ad85b36ecd",
            "compositeImage": {
                "id": "c542dcf8-7099-4389-9409-28835b92cc73",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a46a8891-5b5f-4f7c-bdf3-2f3f3620bb5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4440c87e-6063-4c73-a06e-21f07ac65fe0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a46a8891-5b5f-4f7c-bdf3-2f3f3620bb5b",
                    "LayerId": "ddaa652b-c0ad-43ac-9f0a-6336d88e0f65"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 313,
    "layers": [
        {
            "id": "ddaa652b-c0ad-43ac-9f0a-6336d88e0f65",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "93614bec-f304-439e-8d2a-a0ad85b36ecd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 203,
    "xorig": 101,
    "yorig": 312
}