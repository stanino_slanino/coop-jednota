{
    "id": "6f37d94f-461d-4689-b822-229c1d6cbbd3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_babka",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 596,
    "bbox_left": 62,
    "bbox_right": 487,
    "bbox_top": 7,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "03222bc0-7e5a-4d8a-92ca-a29e209182f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6f37d94f-461d-4689-b822-229c1d6cbbd3",
            "compositeImage": {
                "id": "e95d8f5e-5804-4769-8e6d-6bb12564e857",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03222bc0-7e5a-4d8a-92ca-a29e209182f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4003ee6c-211b-4ffc-8ed4-173f2482d84b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03222bc0-7e5a-4d8a-92ca-a29e209182f0",
                    "LayerId": "92082723-ce5f-4bd7-a469-9c9fd472cef7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 671,
    "layers": [
        {
            "id": "92082723-ce5f-4bd7-a469-9c9fd472cef7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6f37d94f-461d-4689-b822-229c1d6cbbd3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 540,
    "xorig": 270,
    "yorig": 670
}