{
    "id": "32f9c303-6118-441e-97a5-648b7ca49d70",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_can",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 303,
    "bbox_left": 0,
    "bbox_right": 172,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "525ae5be-0ec2-4fe7-8fc3-28729564c53d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "32f9c303-6118-441e-97a5-648b7ca49d70",
            "compositeImage": {
                "id": "400c5cd2-3786-4b1d-bc87-92f43162dd49",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "525ae5be-0ec2-4fe7-8fc3-28729564c53d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "22af0fdb-726d-46d4-a632-f1827ad04e26",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "525ae5be-0ec2-4fe7-8fc3-28729564c53d",
                    "LayerId": "590c4da4-2f8d-44a4-a15e-082bba4e2092"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 304,
    "layers": [
        {
            "id": "590c4da4-2f8d-44a4-a15e-082bba4e2092",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "32f9c303-6118-441e-97a5-648b7ca49d70",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 173,
    "xorig": 86,
    "yorig": 303
}