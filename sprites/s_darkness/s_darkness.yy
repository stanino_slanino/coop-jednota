{
    "id": "1bc96789-3217-489c-becb-ecd7c29b1f63",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_darkness",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 359,
    "bbox_left": 465,
    "bbox_right": 1436,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8c6bfce1-960a-49c9-8761-28724c8f13ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1bc96789-3217-489c-becb-ecd7c29b1f63",
            "compositeImage": {
                "id": "e25b55ae-5559-4856-9f95-07bfbdd0a516",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c6bfce1-960a-49c9-8761-28724c8f13ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "22f35a95-8d8b-49c7-ad4e-67c13b2263ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c6bfce1-960a-49c9-8761-28724c8f13ed",
                    "LayerId": "5a1d2eaf-2915-4733-bdf9-4c6bd04ed244"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1080,
    "layers": [
        {
            "id": "5a1d2eaf-2915-4733-bdf9-4c6bd04ed244",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1bc96789-3217-489c-becb-ecd7c29b1f63",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1920,
    "xorig": 0,
    "yorig": 0
}