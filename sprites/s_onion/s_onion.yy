{
    "id": "198925f2-d3b4-4c8c-8188-44eb8ea8dac0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_onion",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 229,
    "bbox_left": 0,
    "bbox_right": 219,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4f335844-1011-4322-a76a-c664f0e08992",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "198925f2-d3b4-4c8c-8188-44eb8ea8dac0",
            "compositeImage": {
                "id": "3caac242-b7da-4229-91c7-b74454b08e66",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f335844-1011-4322-a76a-c664f0e08992",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49d2ab1c-aa0a-4f6b-beec-71e6bfbdd8c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f335844-1011-4322-a76a-c664f0e08992",
                    "LayerId": "d992b303-fcdb-4fbb-ad11-4d6c194f40d7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 230,
    "layers": [
        {
            "id": "d992b303-fcdb-4fbb-ad11-4d6c194f40d7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "198925f2-d3b4-4c8c-8188-44eb8ea8dac0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 220,
    "xorig": 110,
    "yorig": 229
}