{
    "id": "a5851789-0486-4718-ae7b-46115521263c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_pear",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 261,
    "bbox_left": 0,
    "bbox_right": 218,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6a5323cb-043e-4084-b282-38d03eeb1bb8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5851789-0486-4718-ae7b-46115521263c",
            "compositeImage": {
                "id": "465c056d-912d-46c1-a006-27415dfd1fe3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a5323cb-043e-4084-b282-38d03eeb1bb8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57c4d61e-f84f-4040-a687-081908cd6ce3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a5323cb-043e-4084-b282-38d03eeb1bb8",
                    "LayerId": "f74e21df-9057-4857-ab58-f0ae39a0073f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 262,
    "layers": [
        {
            "id": "f74e21df-9057-4857-ab58-f0ae39a0073f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a5851789-0486-4718-ae7b-46115521263c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 219,
    "xorig": 109,
    "yorig": 261
}