{
    "id": "2cdbba12-467d-40b2-a81f-e28cf5add464",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_polica_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1257,
    "bbox_left": 141,
    "bbox_right": 407,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0bb8edb3-c6da-4f80-b7f0-4c78a43dc0d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2cdbba12-467d-40b2-a81f-e28cf5add464",
            "compositeImage": {
                "id": "e8f35608-06ee-4c30-a756-972777191a95",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0bb8edb3-c6da-4f80-b7f0-4c78a43dc0d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0ba9e6a1-377a-4949-bedd-66e9bd369c18",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0bb8edb3-c6da-4f80-b7f0-4c78a43dc0d6",
                    "LayerId": "a49de1a8-6cff-4455-95b9-02b2ecd7faf1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1253,
    "layers": [
        {
            "id": "a49de1a8-6cff-4455-95b9-02b2ecd7faf1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2cdbba12-467d-40b2-a81f-e28cf5add464",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 8,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 408,
    "xorig": 407,
    "yorig": 1252
}