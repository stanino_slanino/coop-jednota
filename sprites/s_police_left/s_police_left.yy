{
    "id": "737b9fad-85ef-4969-8f7a-cac40a507e3c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_police_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1257,
    "bbox_left": 0,
    "bbox_right": 278,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0a00a7a9-a77c-4ad9-a171-8db00d40a188",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "737b9fad-85ef-4969-8f7a-cac40a507e3c",
            "compositeImage": {
                "id": "f24772d5-a9fb-4a93-a5ce-f4016ce267c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a00a7a9-a77c-4ad9-a171-8db00d40a188",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e6be4b63-fce7-4cff-8c3d-a8c6b4b691a0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a00a7a9-a77c-4ad9-a171-8db00d40a188",
                    "LayerId": "050f52c6-6b42-4d92-9e8d-85c2464ac680"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1253,
    "layers": [
        {
            "id": "050f52c6-6b42-4d92-9e8d-85c2464ac680",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "737b9fad-85ef-4969-8f7a-cac40a507e3c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 6,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 408,
    "xorig": 0,
    "yorig": 1252
}