{
    "id": "fe5ae1c2-fd6c-4a7f-aabc-97039909d788",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_steny",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 990,
    "bbox_left": 0,
    "bbox_right": 1919,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e4bd9368-8173-48bc-b7a2-7ed15d5b542d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fe5ae1c2-fd6c-4a7f-aabc-97039909d788",
            "compositeImage": {
                "id": "19d63b00-c139-471e-adb6-9685a057ec07",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4bd9368-8173-48bc-b7a2-7ed15d5b542d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "14b9fe89-be92-4512-8850-6d509ab4d08d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4bd9368-8173-48bc-b7a2-7ed15d5b542d",
                    "LayerId": "7f4c4be4-162e-4b1c-b0ad-37a5e33529fd"
                }
            ]
        },
        {
            "id": "8e60314b-7c25-4015-9222-b909e28ad897",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fe5ae1c2-fd6c-4a7f-aabc-97039909d788",
            "compositeImage": {
                "id": "50a34436-f5f3-4bf8-8547-8f9d15589a45",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e60314b-7c25-4015-9222-b909e28ad897",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4279ba6-610b-4618-b51f-06ce9b7dd6da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e60314b-7c25-4015-9222-b909e28ad897",
                    "LayerId": "7f4c4be4-162e-4b1c-b0ad-37a5e33529fd"
                }
            ]
        },
        {
            "id": "352374df-6b2e-4af7-91ed-e15bc2b70811",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fe5ae1c2-fd6c-4a7f-aabc-97039909d788",
            "compositeImage": {
                "id": "e8de49a5-4522-4e0d-aaeb-fc11b4a98074",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "352374df-6b2e-4af7-91ed-e15bc2b70811",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0d5e323-fc95-4e67-a3cf-b8dec2729187",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "352374df-6b2e-4af7-91ed-e15bc2b70811",
                    "LayerId": "7f4c4be4-162e-4b1c-b0ad-37a5e33529fd"
                }
            ]
        },
        {
            "id": "8d35472f-15bd-45d5-96b1-6b54d0f4b3c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fe5ae1c2-fd6c-4a7f-aabc-97039909d788",
            "compositeImage": {
                "id": "fdce1657-e6e7-463d-8374-7ab567b54559",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d35472f-15bd-45d5-96b1-6b54d0f4b3c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "367a50bc-cd49-491f-a288-51407c2edad4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d35472f-15bd-45d5-96b1-6b54d0f4b3c6",
                    "LayerId": "7f4c4be4-162e-4b1c-b0ad-37a5e33529fd"
                }
            ]
        },
        {
            "id": "6df4ca55-1557-449b-a00d-a5d1ca4e0456",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fe5ae1c2-fd6c-4a7f-aabc-97039909d788",
            "compositeImage": {
                "id": "7bb5e101-9499-459a-9fa3-c5be2a0c9050",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6df4ca55-1557-449b-a00d-a5d1ca4e0456",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5c70f66-dd13-448b-b6e9-c4952e2f58e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6df4ca55-1557-449b-a00d-a5d1ca4e0456",
                    "LayerId": "7f4c4be4-162e-4b1c-b0ad-37a5e33529fd"
                }
            ]
        },
        {
            "id": "b8b69fa9-5e46-40a7-8d02-bbd1998ba4a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fe5ae1c2-fd6c-4a7f-aabc-97039909d788",
            "compositeImage": {
                "id": "e3431609-3d82-4e76-8a61-672ca7f19882",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8b69fa9-5e46-40a7-8d02-bbd1998ba4a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57802ed7-adc4-4a90-aa30-f75d4265ecf8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8b69fa9-5e46-40a7-8d02-bbd1998ba4a5",
                    "LayerId": "7f4c4be4-162e-4b1c-b0ad-37a5e33529fd"
                }
            ]
        },
        {
            "id": "77d1b13d-9e97-4867-bd59-eba250311c03",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fe5ae1c2-fd6c-4a7f-aabc-97039909d788",
            "compositeImage": {
                "id": "08679aae-c6ec-4740-a7e4-a63416d60cd8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77d1b13d-9e97-4867-bd59-eba250311c03",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9badbd69-73f0-4031-9c29-12987757d233",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77d1b13d-9e97-4867-bd59-eba250311c03",
                    "LayerId": "7f4c4be4-162e-4b1c-b0ad-37a5e33529fd"
                }
            ]
        },
        {
            "id": "536515a5-3d83-4406-bdfe-3dcb52ec3be1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fe5ae1c2-fd6c-4a7f-aabc-97039909d788",
            "compositeImage": {
                "id": "d36d56f7-4c18-45b1-908f-6a004643b26a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "536515a5-3d83-4406-bdfe-3dcb52ec3be1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a34f2523-54b9-457f-82a6-4e0f1edcc941",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "536515a5-3d83-4406-bdfe-3dcb52ec3be1",
                    "LayerId": "7f4c4be4-162e-4b1c-b0ad-37a5e33529fd"
                }
            ]
        },
        {
            "id": "b9dfce2f-4c40-4b20-ba53-13eb6be21b58",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fe5ae1c2-fd6c-4a7f-aabc-97039909d788",
            "compositeImage": {
                "id": "be1c5dc5-f9cf-48a2-a7cb-0c35ffa99151",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9dfce2f-4c40-4b20-ba53-13eb6be21b58",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1475632b-b16b-478c-8775-fdab3ef43d37",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9dfce2f-4c40-4b20-ba53-13eb6be21b58",
                    "LayerId": "7f4c4be4-162e-4b1c-b0ad-37a5e33529fd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1080,
    "layers": [
        {
            "id": "7f4c4be4-162e-4b1c-b0ad-37a5e33529fd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fe5ae1c2-fd6c-4a7f-aabc-97039909d788",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1920,
    "xorig": 0,
    "yorig": 0
}