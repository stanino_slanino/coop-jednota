{
    "id": "c17ab4a3-ae81-46ba-bc32-8af1cd195a70",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_tabulka",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 236,
    "bbox_left": 27,
    "bbox_right": 302,
    "bbox_top": 137,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ecb23898-2201-4621-bd93-fc4c06beee32",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c17ab4a3-ae81-46ba-bc32-8af1cd195a70",
            "compositeImage": {
                "id": "d91e998c-3baf-4a60-8a49-ab428592617d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ecb23898-2201-4621-bd93-fc4c06beee32",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "693fe963-31b4-44d0-9329-6035164ebb84",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ecb23898-2201-4621-bd93-fc4c06beee32",
                    "LayerId": "f757ebbe-363b-410a-be6f-f7c0ea2e91cc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 306,
    "layers": [
        {
            "id": "f757ebbe-363b-410a-be6f-f7c0ea2e91cc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c17ab4a3-ae81-46ba-bc32-8af1cd195a70",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 333,
    "xorig": 166,
    "yorig": 305
}