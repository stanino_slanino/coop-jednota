{
    "id": "fe6c6639-a188-4e61-8448-9ce64e294606",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 575,
    "bbox_left": 0,
    "bbox_right": 191,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a6727067-51ae-4b1a-827d-e689b27c670f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fe6c6639-a188-4e61-8448-9ce64e294606",
            "compositeImage": {
                "id": "e76758a7-47dd-4dd7-875b-a3a9652ff7b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6727067-51ae-4b1a-827d-e689b27c670f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0619ace-0089-47f7-be39-e9e2f3addbcd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6727067-51ae-4b1a-827d-e689b27c670f",
                    "LayerId": "cdf4c2cf-c7d7-44f3-9c84-0674e5b681cb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 576,
    "layers": [
        {
            "id": "cdf4c2cf-c7d7-44f3-9c84-0674e5b681cb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fe6c6639-a188-4e61-8448-9ce64e294606",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 192,
    "xorig": 96,
    "yorig": 575
}