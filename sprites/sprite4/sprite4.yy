{
    "id": "ebd14139-c030-4e83-b015-56adc6d9b2d7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 191,
    "bbox_left": 0,
    "bbox_right": 383,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9e25219f-c86c-408a-8fbc-fa6a08ed3d03",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebd14139-c030-4e83-b015-56adc6d9b2d7",
            "compositeImage": {
                "id": "93b30f87-242a-4633-ad55-f1f9601fa091",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e25219f-c86c-408a-8fbc-fa6a08ed3d03",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aebdb58b-03c8-4216-9858-f4066b646ef1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e25219f-c86c-408a-8fbc-fa6a08ed3d03",
                    "LayerId": "e3403186-423a-4f3e-acd5-a26866f60d29"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 192,
    "layers": [
        {
            "id": "e3403186-423a-4f3e-acd5-a26866f60d29",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ebd14139-c030-4e83-b015-56adc6d9b2d7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 384,
    "xorig": 192,
    "yorig": 191
}