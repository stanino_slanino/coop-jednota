{
    "id": "a3566b83-66a5-468f-a892-f6aabcd55053",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite5",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 191,
    "bbox_left": 0,
    "bbox_right": 575,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ab4461c4-15f0-4498-ad93-632e5cb623ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a3566b83-66a5-468f-a892-f6aabcd55053",
            "compositeImage": {
                "id": "137911de-466e-42a5-880f-76d9f63c5d4b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab4461c4-15f0-4498-ad93-632e5cb623ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "edd5bb34-08c1-4465-8da8-98c374c67130",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab4461c4-15f0-4498-ad93-632e5cb623ca",
                    "LayerId": "c8a6bcf8-8fad-4031-bed5-3f8d11b33484"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 192,
    "layers": [
        {
            "id": "c8a6bcf8-8fad-4031-bed5-3f8d11b33484",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a3566b83-66a5-468f-a892-f6aabcd55053",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 576,
    "xorig": 288,
    "yorig": 191
}