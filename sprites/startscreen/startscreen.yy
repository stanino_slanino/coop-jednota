{
    "id": "78c5cfd9-e3fc-4013-9e64-2bd63ea56cc2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "startscreen",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1079,
    "bbox_left": 0,
    "bbox_right": 1919,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b25cbed4-f508-44a4-aed3-14c97244f1c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78c5cfd9-e3fc-4013-9e64-2bd63ea56cc2",
            "compositeImage": {
                "id": "43261738-2205-4903-9065-bb0aed32fc3c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b25cbed4-f508-44a4-aed3-14c97244f1c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "261832d0-03a9-4956-a621-0b773eba7bf3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b25cbed4-f508-44a4-aed3-14c97244f1c2",
                    "LayerId": "877ba855-2952-4641-9bf5-28c914b1e5ae"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1080,
    "layers": [
        {
            "id": "877ba855-2952-4641-9bf5-28c914b1e5ae",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "78c5cfd9-e3fc-4013-9e64-2bd63ea56cc2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1920,
    "xorig": 0,
    "yorig": 0
}