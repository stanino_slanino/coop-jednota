{
    "id": "70093695-662b-405a-8dea-4979eade46fc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "vybuch",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 208,
    "bbox_left": 0,
    "bbox_right": 299,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c8678dc0-808e-4bd1-8e9d-33dcd8544aeb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "70093695-662b-405a-8dea-4979eade46fc",
            "compositeImage": {
                "id": "26250727-dd83-48eb-b133-2cc42ecf87f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8678dc0-808e-4bd1-8e9d-33dcd8544aeb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4ad0b15-46af-47ff-ab80-409b00952838",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8678dc0-808e-4bd1-8e9d-33dcd8544aeb",
                    "LayerId": "d2ed18b1-ec48-4b67-b6dd-8a1d2a19bf31"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 209,
    "layers": [
        {
            "id": "d2ed18b1-ec48-4b67-b6dd-8a1d2a19bf31",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "70093695-662b-405a-8dea-4979eade46fc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 300,
    "xorig": 150,
    "yorig": 208
}